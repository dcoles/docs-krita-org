# Translation of docs_krita_org_general_concepts___colors___bit_depth.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-24 19:20+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../general_concepts/colors/bit_depth.rst:None
msgid ".. image:: images/color_category/Kiki_lowbit.png"
msgstr ".. image:: images/color_category/Kiki_lowbit.png"

#: ../../general_concepts/colors/bit_depth.rst:1
msgid "Bit depth in Krita."
msgstr "Profunditat de bits en el Krita."

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:15
msgid "Bit Depth"
msgstr "Profunditat de bits"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:26
msgid "Indexed Color"
msgstr "Color indexat"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:46
msgid "Real Color"
msgstr "Color real"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color"
msgstr "Color"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Bit Depth"
msgstr "Profunditat de bits del color"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Deep Color"
msgstr "Profunditat de color"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Floating Point Color"
msgstr "Color de coma flotant"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Channels"
msgstr "Canals de color"

#: ../../general_concepts/colors/bit_depth.rst:17
msgid ""
"Bit depth basically refers to the amount of working memory per pixel you "
"reserve for an image."
msgstr ""
"La profunditat de bits bàsicament es refereix a la quantitat de memòria de "
"treball per píxel que reserveu per a una imatge."

#: ../../general_concepts/colors/bit_depth.rst:19
msgid ""
"Like how having a A2 paper in real life can allow for much more detail in "
"the end drawing, it does take up more of your desk than a simple A4 paper."
msgstr ""
"Igual que el fet de tenir un paper A2 en la vida real pot permetre molt més "
"detall en el dibuix final, ja que ocupa més del vostre escriptori que un "
"paper A4."

#: ../../general_concepts/colors/bit_depth.rst:21
msgid ""
"However, this does not just refer to the size of the image, but also how "
"much precision you need per color."
msgstr ""
"No obstant això, no només es refereix a la mida de la imatge, sinó també a "
"la quantitat de precisió que necessiteu per al color."

#: ../../general_concepts/colors/bit_depth.rst:23
msgid ""
"To illustrate this, I'll briefly talk about something that is not even "
"available in Krita:"
msgstr ""
"Per il·lustrar-ho, parlaré breument sobre quelcom que ni tan sols està "
"disponible en el Krita:"

#: ../../general_concepts/colors/bit_depth.rst:28
msgid ""
"In older programs, the computer would have per image, a palette that "
"contains a number for each color. The palette size is defined in bits, "
"because the computer can only store data in bit-sizes."
msgstr ""
"En els programes antics, l'ordinador el tindríeu per imatge, una paleta que "
"conté un número per a cada color. La mida de la paleta es defineix en bits, "
"perquè l'ordinador només pot emmagatzemar les dades en mides de bits."

#: ../../general_concepts/colors/bit_depth.rst:36
msgid "1bit"
msgstr "1 bit"

#: ../../general_concepts/colors/bit_depth.rst:37
msgid "Only two colors in total, usually black and white."
msgstr "Només dos colors en total, generalment en blanc i negre."

#: ../../general_concepts/colors/bit_depth.rst:38
msgid "4bit (16 colors)"
msgstr "4 bits (16 colors)"

#: ../../general_concepts/colors/bit_depth.rst:39
msgid ""
"16 colors in total, these are famous as many early games were presented in "
"this color palette."
msgstr ""
"16 colors en total, aquests són famosos ja que molts dels primers jocs van "
"ser presentats en aquesta paleta de colors."

#: ../../general_concepts/colors/bit_depth.rst:41
msgid "8bit"
msgstr "8 bits"

#: ../../general_concepts/colors/bit_depth.rst:41
msgid ""
"256 colors in total. 8bit images are commonly used in games to save on "
"memory for textures and sprites."
msgstr ""
"256 colors en total. Les imatges de 8 bits s'utilitzen habitualment en jocs "
"per desar a la memòria per a textures i franges."

#: ../../general_concepts/colors/bit_depth.rst:43
msgid ""
"However, this is not available in Krita. Krita instead works with channels, "
"and counts how many colors per channel you need (described in terms of "
"''bits per channel''). This is called 'real color'."
msgstr ""
"No obstant això, no està disponible en el Krita. El Krita, en canvi, "
"treballa amb canals i compta la quantitat de colors per canal que necessiteu "
"(descrit en termes de «bits per canal»). Això es diu «color real»."

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ".. image:: images/color_category/Rgbcolorcube_3.png"
msgstr ".. image:: images/color_category/Rgbcolorcube_3.png"

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ""
"1, 2, and 3bit per channel don't actually exist in any graphics application "
"out there, however, by imagining them, we can imagine how each bit affects "
"the precision: Usually, each bit subdivides each section in the color cube "
"meaning precision becomes a power of 2 bigger than the previous cube."
msgstr ""
"Els 1, 2 i 3 bits per canal en realitat no existeixen en cap aplicació de "
"gràfics, però en imaginar-los, podrem imaginar com afectarà cada bit a la "
"precisió: en general, cada bit subdivideix cada secció al cub de color, el "
"qual voldrà dir precisió i una potència de 2 més gran que el cub anterior."

#: ../../general_concepts/colors/bit_depth.rst:54
msgid "4bit per channel (not supported by Krita)"
msgstr "4 bits per canal (no admès pel Krita)"

#: ../../general_concepts/colors/bit_depth.rst:55
msgid ""
"Also known as Hi-Color, or 16bit color total. A bit of an old system, and "
"not used outside of specific displays."
msgstr ""
"També es coneix com a color d'alta qualitat (Hi-Color) o total de color de "
"16 bits. És un sistema relativament antic i no s'utilitza fora de les "
"pantalles específiques."

#: ../../general_concepts/colors/bit_depth.rst:56
msgid "8bit per channel"
msgstr "8 bits per canal"

#: ../../general_concepts/colors/bit_depth.rst:57
msgid ""
"Also known as \"True Color\", \"Millions of colors\" or \"24bit/32bit\". The "
"standard for many screens, and the lowest bit-depth Krita can handle."
msgstr ""
"També es coneix com a «color veritable» (True Color), «milions de colors» o "
"«24/32 bits». L'estàndard per a moltes pantalles i la més baixa profunditat "
"de bits que pot gestionar el Krita."

#: ../../general_concepts/colors/bit_depth.rst:58
msgid "16bit per channel"
msgstr "16 bits per canal"

#: ../../general_concepts/colors/bit_depth.rst:59
msgid ""
"One step up from 8bit, 16bit per channel allows for colors that can't be "
"displayed by the screen. However, due to this, you are more likely to have "
"smoother gradients. Sometimes known as \"Deep Color\". This color depth type "
"doesn't have negative values possible, so it is 16bit precision, meaning "
"that you have 65536 values per channel."
msgstr ""
"Un pas endavant dels 8 bits, els 16 bits per canal permeten colors que la "
"pantalla no pot mostrar. No obstant això, a causa d'això, és més probable "
"que tingui degradats més suaus. De vegades conegut com a «color "
"intens» (Deep Color). Aquest tipus de profunditat de color no té valors "
"negatius possibles, de manera que té una precisió de 16 bits, el qual vol "
"dir que té 65.536 valors per canal."

#: ../../general_concepts/colors/bit_depth.rst:60
msgid "16bit float"
msgstr "Flotant de 16 bits"

#: ../../general_concepts/colors/bit_depth.rst:61
msgid ""
"Similar to 16bit, but with more range and less precision. Where 16bit only "
"allows coordinates like [1, 4, 3], 16bit float has coordinates like [0.15, "
"0.70, 0.3759], with [1.0,1.0,1.0] representing white. Because of the "
"differences between floating point and integer type variables, and because "
"Scene-referred imaging allows for negative values, you have about 10-11bits "
"of precision per channel in 16 bit floating point compared to 16 bit in 16 "
"bit int (this is 2048 values per channel in the 0-1 range). Required for HDR/"
"Scene referred images, and often known as 'half floating point'."
msgstr ""
"Similar als 16 bits, però amb més abast i menys precisió. On els 16 bits "
"només permeten coordenades com [1, 4, 3], els flotants de 16 bits tenen "
"coordenades com [0,15, 0,70, 0,3759], amb [1,0,1,0,1,0] representant el "
"blanc. A causa de les diferències entre les variables de tipus en coma "
"flotant i sencer, i pel fet que les imatges en referència a l'escena "
"permeten valors negatius, tindreu aproximadament de 10 a 11 bits de precisió "
"per canal en un coma flotant de 16 bits comparat amb els 16 bits en sencers "
"de 16 bits (això són 2.048 valors per canal en l'interval de 0 a 1). "
"Requerit per les imatges HDR/en referència a l'escena, i sovint conegut com "
"a «coma flotant mitja»."

#: ../../general_concepts/colors/bit_depth.rst:63
msgid ""
"Similar to 16bit float but with even higher precision. The native color "
"depth of OpenColor IO, and thus faster than 16bit float in HDR images, if "
"not heavier. Because of the nature of floating point type variables, 32bit "
"float is roughly equal to 23-24 bits of precision per channel (16777216 "
"values per channel in the 0-1 range), but with a much wider range (it can go "
"far above 1), necessary for HDR/Scene-referred values. It is also known as "
"'single floating point'."
msgstr ""
"Similar al flotant de 16 bits però amb una precisió encara més gran. La "
"profunditat de color nativa de l'E/S d'OpenColor i, per tant, més ràpida que "
"el flotant de 16 bits en les imatges HDR, si no més pesades. A causa de la "
"naturalesa de les variables de tipus de coma flotant, el flotant de 32 bits "
"és aproximadament igual que els 23-24 bits de precisió per canal (16.777.216 "
"valors per canal en l'interval de 0 a 1), però amb un ventall més ampli (pot "
"anar molt per sobre d'1), necessari per al HDR/Valors en referència a "
"l'escena. També es coneix com a «coma flotant única»."

#: ../../general_concepts/colors/bit_depth.rst:64
msgid "32bit float"
msgstr "Flotant de 32 bits"

#: ../../general_concepts/colors/bit_depth.rst:66
msgid ""
"This is important if you have a working color space that is larger than your "
"device space: At the least, if you do not want color banding."
msgstr ""
"Això serà important si teniu un espai de color de treball que és més gran "
"que l'espai en el vostre dispositiu: com a mínim, si no voleu un efecte de "
"cartell en el color."

#: ../../general_concepts/colors/bit_depth.rst:68
msgid ""
"And while you can attempt to create all your images a 32bit float, this will "
"quickly take up your RAM. Therefore, it's important to consider which bit "
"depth you will use for what kind of image."
msgstr ""
"I si bé podreu intentar crear totes les vostres imatges amb un flotant de 32 "
"bits, això ocuparà la RAM. Per tant, és important tenir en compte quina "
"profunditat de bits s'utilitzarà per a quin tipus d'imatge."
