# Translation of docs_krita_org_reference_manual___filters___edge_detection.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 20:34+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "XYZ"
msgstr "XYZ"

#: ../../reference_manual/filters/edge_detection.rst:None
msgid ".. image:: images/filters/Krita_4_0_height_to_normal_map.png"
msgstr ".. image:: images/filters/Krita_4_0_height_to_normal_map.png"

#: ../../reference_manual/filters/edge_detection.rst:1
msgid "Overview of the edge detection filters."
msgstr "Vista general dels filtres de detecció de les vores."

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:16
#: ../../reference_manual/filters/edge_detection.rst:23
msgid "Edge Detection"
msgstr "Detecció de les vores"

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:40
#: ../../reference_manual/filters/edge_detection.rst:83
msgid "Prewitt"
msgstr "Prewitt"

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid "Sobel"
msgstr "Sobel"

#: ../../reference_manual/filters/edge_detection.rst:18
msgid ""
"Edge detection filters focus on finding sharp contrast or border between "
"colors in an image to create edges or lines."
msgstr ""
"Els filtres per a la detecció de les vores se centren en trobar un contrast "
"més definit o una vora entre els colors d'una imatge per a crear vores o "
"línies."

#: ../../reference_manual/filters/edge_detection.rst:20
msgid "Since 4.0 there are only two edge detection filters."
msgstr ""
"Des de la versió 4.0 només hi ha dos filtres per a la detecció de les vores."

#: ../../reference_manual/filters/edge_detection.rst:27
msgid ""
"A general edge detection filter that encapsulates all other filters. Edge "
"detection filters that were separate before 4.0 have been folded into this "
"one. It is also available for filter layers and filter brushes."
msgstr ""
"Un filtre general per a la detecció de les vores que encapsula tots els "
"altres filtres. Els filtres per a la detecció de les vores que estaven "
"separats abans de la versió 4.0 s'han integrat en aquest. També estarà "
"disponible per a les capes de filtratge i els pinzells amb filtratge."

#: ../../reference_manual/filters/edge_detection.rst:33
msgid ".. image:: images/filters/Krita_4_0_edge_detection.png"
msgstr ".. image:: images/filters/Krita_4_0_edge_detection.png"

#: ../../reference_manual/filters/edge_detection.rst:33
msgid ""
"From left to right: Original, with Prewitt edge detection applied, with "
"Prewitt edge detection applied and result applied to alpha channel, and "
"finally the original with an edge detection filter layer with the same "
"settings as 3, and the filter layer blending mode set to multiply"
msgstr ""
"D'esquerra a dreta: 1.- l'original, 2.- amb la detecció de les vores Prewitt "
"aplicada, 3.- amb la detecció de les vores Prewitt aplicada i el resultat "
"aplicat al canal alfa, 4.- i finalment amb la detecció de les vores amb els "
"mateixos ajustaments que 3, i el mode de barreja de la capa de filtratge "
"establert a multiplica."

#: ../../reference_manual/filters/edge_detection.rst:36
#: ../../reference_manual/filters/edge_detection.rst:79
msgid ""
"The convolution kernel formula for the edge detection. The difference "
"between these is subtle, but still worth experimenting with."
msgstr ""
"La fórmula del nucli de la convolució per a la detecció de les vores. La "
"diferència entre aquests nuclis és subtil, però tot i així val la pena "
"experimentar."

#: ../../reference_manual/filters/edge_detection.rst:38
#: ../../reference_manual/filters/edge_detection.rst:81
msgid "Simple"
msgstr "Senzill"

#: ../../reference_manual/filters/edge_detection.rst:39
#: ../../reference_manual/filters/edge_detection.rst:82
msgid ""
"A Kernel that is not square unlike the other two, and while this makes it "
"fast, it doesn't take diagonal pixels into account."
msgstr ""
"Un nucli que no és quadrat a diferència dels altres dos, i encara que ho fa "
"ràpid, no té en compte els píxels en diagonal."

#: ../../reference_manual/filters/edge_detection.rst:41
#: ../../reference_manual/filters/edge_detection.rst:84
msgid ""
"A square kernel that includes the diagonal pixels just as strongly as the "
"orthogonal pixels. Gives a very strong effect."
msgstr ""
"Un nucli quadrat que inclou els píxels en diagonal amb la mateixa força els "
"píxels en ortogonal. Dóna un efecte molt fort."

#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid "Formula"
msgstr "Fórmula"

#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid ""
"A square kernel that includes the diagonal pixels slightly less strong than "
"the orthogonal pixels. Gives a more subtle effect than Prewitt."
msgstr ""
"Un nucli quadrat que inclou els píxels en diagonal amb una mica menys de "
"força que els píxels en ortogonal. Dóna un efecte més subtil que el de "
"Prewitt."

#: ../../reference_manual/filters/edge_detection.rst:46
msgid "The output."
msgstr "La sortida."

#: ../../reference_manual/filters/edge_detection.rst:48
msgid "All sides"
msgstr "Tots els costats"

#: ../../reference_manual/filters/edge_detection.rst:49
msgid ""
"Convolves the edge detection into all directions and combines the result "
"with the Pythagorean theorem. This will be good for most uses."
msgstr ""
"Converteix la detecció de les vores en totes les direccions i combina el "
"resultat amb el teorema de Pitàgores. Serà bo per a la majoria dels casos "
"d'ús."

#: ../../reference_manual/filters/edge_detection.rst:50
msgid "Top Edge"
msgstr "Vora superior"

#: ../../reference_manual/filters/edge_detection.rst:51
msgid ""
"This only detects changes going from top to bottom and thus only has top "
"lines."
msgstr ""
"Només detecta els canvis que van de dalt a baix, de manera que només tindrà "
"línies a la part superior."

#: ../../reference_manual/filters/edge_detection.rst:52
msgid "Bottom Edge"
msgstr "Vora inferior"

#: ../../reference_manual/filters/edge_detection.rst:53
msgid ""
"This only detects changes going from bottom to top and thus only has bottom "
"lines."
msgstr ""
"Només detecta els canvis que van de baix a dalt, de manera que només tindrà "
"línies a la part inferior."

#: ../../reference_manual/filters/edge_detection.rst:54
msgid "Right Edge"
msgstr "Vora dreta"

#: ../../reference_manual/filters/edge_detection.rst:55
msgid ""
"This only detects changes going from right to left and thus only has right "
"lines."
msgstr ""
"Només detecta els canvis que van de dreta a esquerra, de manera que només "
"tindrà línies a la part dreta."

#: ../../reference_manual/filters/edge_detection.rst:56
msgid "Left Edge"
msgstr "Vora esquerra"

#: ../../reference_manual/filters/edge_detection.rst:57
msgid ""
"This only detects changes going from left to right and thus only has left "
"lines."
msgstr ""
"Només detecta els canvis que van d'esquerra a dreta, de manera que només "
"tindrà línies a la part esquerra."

#: ../../reference_manual/filters/edge_detection.rst:59
msgid "Output"
msgstr "Sortida"

#: ../../reference_manual/filters/edge_detection.rst:59
msgid "Direction in Radians"
msgstr "Direcció en radians"

#: ../../reference_manual/filters/edge_detection.rst:59
msgid ""
"This convolves into all directions and then tries to output the direction of "
"the line in radians."
msgstr ""
"Converteix en totes les direccions i després intenta donar a la sortida la "
"direcció de la línia en radians."

#: ../../reference_manual/filters/edge_detection.rst:61
#: ../../reference_manual/filters/edge_detection.rst:90
msgid "Horizontal/Vertical radius"
msgstr "Radi horitzontal/vertical"

#: ../../reference_manual/filters/edge_detection.rst:62
msgid ""
"The radius of the edge detection. Default is 1 and going higher will "
"increase the thickness of the lines."
msgstr ""
"El radi per a la detecció de les vores. El valor predeterminat és 1 i en fer-"
"lo més alt augmentarà el gruix de les línies."

# skip-rule: punctuation-period
#: ../../reference_manual/filters/edge_detection.rst:64
msgid "Apply result to Alpha Channel."
msgstr "Aplica el resultat al canal alfa"

#: ../../reference_manual/filters/edge_detection.rst:64
msgid ""
"The edge detection will be used on a grayscale copy of the image, and the "
"output will be onto the alpha channel of the image, meaning it will output "
"lines only."
msgstr ""
"La detecció de les vores s'utilitzarà en una còpia en escala de grisos de la "
"imatge, i la sortida serà en el canal alfa de la imatge, el qual vol dir que "
"només generarà línies."

#: ../../reference_manual/filters/edge_detection.rst:66
msgid "Height Map"
msgstr "Mapa d'alçades"

#: ../../reference_manual/filters/edge_detection.rst:66
msgid "Normal Map"
msgstr "Mapa normal"

#: ../../reference_manual/filters/edge_detection.rst:69
msgid "Height to Normal Map"
msgstr "Mapa d'alçades a normal"

#: ../../reference_manual/filters/edge_detection.rst:76
msgid ""
"A filter that converts Height maps to Normal maps through the power of edge "
"detection. It is also available for the filter layer or filter brush."
msgstr ""
"Un filtre que converteix els Mapes d'alçades en Mapes normals a través del "
"poder de la detecció de les vores. També està disponible per a la capa de "
"filtratge o el pinzell amb filtratge."

#: ../../reference_manual/filters/edge_detection.rst:88
msgid "Channel"
msgstr "Canal"

#: ../../reference_manual/filters/edge_detection.rst:89
msgid ""
"Which channel of the layer should be interpreted as the grayscale heightmap."
msgstr ""
"Quin canal de la capa s'haurà d'interpretar com el mapa d'alçades en escala "
"de grisos."

#: ../../reference_manual/filters/edge_detection.rst:91
msgid ""
"The radius of the edge detection. Default is 1 and going higher will "
"increase the strength of the normal map. Adjust this if the effect of the "
"resulting normal map is too weak."
msgstr ""
"El radi per a la detecció de les vores. El valor predeterminat és 1 i fer-lo "
"més alt augmentarà la intensitat del mapa normal. Ajusteu-lo si l'efecte "
"resultant del mapa normal és massa pobre."

#: ../../reference_manual/filters/edge_detection.rst:93
msgid ""
"An XYZ swizzle, that allows you to map Red, Green and Blue to different 3d "
"normal vector coordinates. This is necessary mostly for the difference "
"between MikkT-space normal maps (+X, +Y, +Z) and the OpenGL standard normal "
"map (+X, -Y, +Z)."
msgstr ""
"Una recombinació dels components XYZ d'un vector, el qual permet assignar el "
"Vermell, Verd i Blau a diferents coordenades vectorials normals en 3D. Això "
"és necessari principalment per la diferència entre els mapes normals en "
"espai de MikkT (+X, +Y, +Z) i el mapa normal estàndard de l'OpenGL (+X, -Y, "
"+Z)."
