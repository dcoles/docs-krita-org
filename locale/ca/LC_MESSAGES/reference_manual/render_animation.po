# Translation of docs_krita_org_reference_manual___render_animation.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 18:17+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/render_animation.rst:1
msgid "How to use the render animation command in Krita."
msgstr "Com s'utilitza l'ordre per a renderitzar l'animació al Krita."

#: ../../reference_manual/render_animation.rst:12
#: ../../reference_manual/render_animation.rst:17
#: ../../reference_manual/render_animation.rst:42
msgid "Render Animation"
msgstr "Renderitzar l'animació"

#: ../../reference_manual/render_animation.rst:12
msgid "Animation"
msgstr "Animació"

#: ../../reference_manual/render_animation.rst:19
msgid ""
"Render animation allows you to render your animation to an image sequence, "
"gif, mp4, mkv, or ogg file. It replaces :guilabel:`Export Animation`."
msgstr ""
"La renderització de l'animació permet renderitzar la vostra animació a una "
"seqüència d'imatges, un fitxer GIF, MP4, MKV o OGG. Substitueix :guilabel:"
"`Exporta l'animació`."

#: ../../reference_manual/render_animation.rst:21
msgid ""
"For rendering to an animated file format, Krita will first render to a png "
"sequence and then use FFmpeg, which is really good at encoding into video "
"files, to render that sequence to an animated file format. The reason for "
"this two-step process is that animation files can be really complex and "
"really big, and this is the best way to allow you to keep control over the "
"export process. For example, if your computer has a hiccup, and one frame "
"saves out weird, first saving the image sequence allows you to only resave "
"that one weird frame before rendering."
msgstr ""
"Per a renderitzar a un format de fitxer animat, el Krita primer renderitzarà "
"a una seqüència PNG i després emprarà el FFmpeg, el qual és realment bo per "
"a codificar dintre de fitxers de vídeo, per a renderitzar aquesta seqüència "
"a un format de fitxer animat. La raó d'aquest procés de dos passos és que "
"els fitxers d'animació poden ser realment complexos i grans, i aquesta és la "
"millor manera de permetre-us mantenir el control sobre el procés de "
"l'exportació. Per exemple, si el vostre ordinador té algun problema, i un "
"fotograma es desa de forma estranya, el desar primer la seqüència d'imatges "
"només us permetrà tornar a desar aquest fotograma estrany abans de fer el "
"renderitzat."

#: ../../reference_manual/render_animation.rst:23
msgid ""
"This means that you will need to find a good place to stick your frames "
"before you can start rendering. If you only do throwaway animations, you can "
"use a spot on your hard-drive with enough room and select :guilabel:`Delete "
"Sequence After Rendering`."
msgstr ""
"Això vol dir que haureu de trobar un bon lloc per enganxar els fotogrames "
"abans de poder començar a renderitzar. Si només creeu animacions d'un sol "
"ús, podeu utilitzar un lloc en el disc dur amb prou espai i seleccionar :"
"guilabel:`Suprimeix la seqüència després de renderitzar`."

#: ../../reference_manual/render_animation.rst:26
msgid "Image Sequence"
msgstr "Seqüència d'imatges"

#: ../../reference_manual/render_animation.rst:28
msgid "Base Name"
msgstr "Nom base"

#: ../../reference_manual/render_animation.rst:29
msgid ""
"The base name of your image sequence. This will get suffixed with a number "
"depending on the frame."
msgstr ""
"El nom base de la seqüència d'imatges. Això obtindrà un sufix amb un número "
"depenent del fotograma."

#: ../../reference_manual/render_animation.rst:30
msgid "File Format"
msgstr "Format de fitxer"

#: ../../reference_manual/render_animation.rst:31
msgid ""
"The file format to export the sequence to. When rendering we enforce png. "
"The usual export options can be modified with :guilabel:`...`."
msgstr ""
"El format de fitxer al qual exportar la seqüència. Durant el renderitzar "
"forçarem PNG. Les opcions d'exportació habituals es poden ajustar amb :"
"guilabel:`...`."

#: ../../reference_manual/render_animation.rst:32
msgid "Render Location"
msgstr "Ubicació de renderitzat"

#: ../../reference_manual/render_animation.rst:33
msgid ""
"Where you render the image sequence to. Some people prefer to use a flash-"
"drive or perhaps a harddrive that is fast."
msgstr ""
"A on renderitzareu la seqüència d'imatges. Hi ha qui prefereix utilitzar una "
"unitat flaix o potser un disc dur que sigui ràpid."

#: ../../reference_manual/render_animation.rst:34
msgid "First Frame"
msgstr "Primer fotograma"

#: ../../reference_manual/render_animation.rst:35
msgid ""
"The first frame of the range of frames you wish to adjust. Automatically set "
"to the first frame of your current selection in the timeline. This is useful "
"when you only want to re-render a little part."
msgstr ""
"El primer fotograma de l'interval de fotogrames que voleu ajustar. "
"S'estableix automàticament al primer fotograma de la vostra selecció actual "
"a la línia de temps. Això és útil quan només voleu tornar a renderitzar una "
"petita part."

#: ../../reference_manual/render_animation.rst:36
msgid "Last Frame"
msgstr "Últim fotograma"

#: ../../reference_manual/render_animation.rst:37
msgid ""
"As above, the last frame of the range of frames you wish to adjust. "
"Automatically set to the last frame of your current selection in the "
"timeline."
msgstr ""
"Com a dalt, l'últim fotograma de l'interval de fotogrames que voleu ajustar. "
"S'establirà automàticament a l'últim fotograma de la vostra selecció actual "
"a la línia de temps."

#: ../../reference_manual/render_animation.rst:39
msgid "Naming Sequence starts with"
msgstr "La seqüència de noms comença amb"

#: ../../reference_manual/render_animation.rst:39
msgid ""
"The frames are named by using :guilabel:`Base Name`  above and adding a "
"number for the frame. This allows you to set where the frame number starts, "
"so rendering from 8 to 10 with starting point 3 will give you images named "
"11 and 15. Useful for programs that don't understand sequences starting with "
"0, or for precision output."
msgstr ""
"Els fotogrames s'anomenaran emprant el :guilabel:`Nom base` anterior i "
"afegint un número per al fotograma. Permetrà establir on comença el nombre "
"de fotogrames, de manera que la representació de 8 a 10 amb el punt d'inici "
"3 us donarà les imatges amb els noms 11 i 15. És útil per a programes que no "
"entenen les seqüències que comencen amb 0 o per a resultats precisos."

#: ../../reference_manual/render_animation.rst:44
msgid "Render As"
msgstr "Renderitza com a"

#: ../../reference_manual/render_animation.rst:45
msgid ""
"The file format to render to. All except gif have extra options that can be "
"manipulated via :guilabel:`...`."
msgstr ""
"El format de fitxer per a renderitzar. Tots excepte el GIF tenen opcions "
"addicionals que poden ser manipulades a través de :guilabel:`...`."

#: ../../reference_manual/render_animation.rst:46
msgid "File"
msgstr "Fitxer"

#: ../../reference_manual/render_animation.rst:47
msgid "Location and name of the rendered animation."
msgstr "Ubicació i nom de l'animació renderitzada."

#: ../../reference_manual/render_animation.rst:48
msgid "FFmpeg"
msgstr "FFmpeg"

#: ../../reference_manual/render_animation.rst:49
msgid ""
"The location where your have FFmpeg. If you don't have this, Krita cannot "
"render an animation. For proper gif support, you will need FFmpeg 2.6, as we "
"use its palettegen functionality."
msgstr ""
"La ubicació on teniu el FFmpeg. Si no el teniu, el Krita no podrà "
"renderitzar una animació. Per a un suport adequat del GIF, necessitareu el "
"FFmpeg 2.6, ja que utilitzem la seva funcionalitat «palettegen»."

#: ../../reference_manual/render_animation.rst:51
msgid "Delete Sequence After Rendering"
msgstr "Suprimeix la seqüència després de renderitzar"

#: ../../reference_manual/render_animation.rst:51
msgid ""
"Delete the prerendered image sequence after done rendering. This allows you "
"to choose whether to try and save some space, or to save the sequence for "
"when encoding fails."
msgstr ""
"Suprimeix la seqüència d'imatges prerenderitzades després d'acabar el "
"renderitzat. Permetrà decidir si voleu estalviar espai o desar la seqüència "
"per a quan falla la codificació."

#: ../../reference_manual/render_animation.rst:55
msgid ""
"None of the video formats support saving from images with a transparent "
"background, so Krita will try to fill it with something. You should add a "
"background color yourself to avoid it from using, say, black."
msgstr ""
"Cap dels formats de vídeo admet el desament d'imatges amb un fons "
"transparent, de manera que el Krita intentarà emplenar-lo amb alguna cosa. "
"Haureu d'afegir un color de fons per evitar que utilitzi, per exemple, el "
"negre."

#: ../../reference_manual/render_animation.rst:58
msgid "Setting Up Krita for Exporting Animations"
msgstr "Configurar el Krita per a exportar les animacions"

#: ../../reference_manual/render_animation.rst:60
msgid ""
"You will need to download an extra application and link it in Krita for it "
"to work. The application is pretty big (50MB), so the Krita developers "
"didn't want to bundle it with the normal application. The software that we "
"will use is free and called FFmpeg. The following instructions will explain "
"how to get it and set it up. The setup is a one-time thing so you won't have "
"to do it again."
msgstr ""
"Us caldrà descarregar una aplicació addicional i vincular-la amb el Krita "
"perquè funcioni. L'aplicació és força gran (50 MB), de manera que els "
"desenvolupadors del Krita no van voler empaquetar-les juntes. El programari "
"que utilitzarem és gratuït i s'anomena FFmpeg. Les següents instruccions "
"explicaran com obtenir-lo i configurar-lo. La configuració serà única, així "
"que no l'haureu de tornar a fer."

#: ../../reference_manual/render_animation.rst:63
msgid "Step 1 - Downloading FFmpeg"
msgstr "Pas 1: descarregar el FFmpeg"

#: ../../reference_manual/render_animation.rst:66
#: ../../reference_manual/render_animation.rst:86
msgid "For Windows"
msgstr "Per a Windows"

# skip-rule: t-acc_obe
#: ../../reference_manual/render_animation.rst:68
msgid ""
"Go to the `FFmpeg website <https://ffmpeg.org/download.html>`_. The URL that "
"had the link for me was `here... <https://ffmpeg.zeranoe.com/builds/>`_"
msgstr ""
"Aneu al `lloc web del FFmpeg <https://ffmpeg.org/download.html>`_. L'URL que "
"tenia de l'enllaç per a mi està `aquí <https://ffmpeg.zeranoe.com/builds/"
">`_..."

#: ../../reference_manual/render_animation.rst:70
msgid ""
"Watch out for the extremely annoying google and that looks like a download "
"button! There is no big button for what we need. Either get the 64-bit "
"STATIC version or 32-bit STATIC version that is shown later down the page. "
"If you bought a computer in the past 5 years, you probably want the 64-bit "
"version. Make sure you get a exe file, if you hover over the options they "
"will give more information about what exactly you are downloading."
msgstr ""
"Compte amb l'extremadament molest Google, sembla un botó de descàrrega! No "
"hi ha cap botó gran per al que necessitem. Obtingueu la versió ESTÀTICA de "
"64 bits o la versió ESTÀTICA de 32 bits que es mostra més endavant de la "
"pàgina. Si heu comprat un ordinador en els últims 5 anys, és probable que "
"vulgueu la versió de 64 bits. Assegureu-vos d'obtenir un fitxer EXE, si "
"passeu el ratolí sobre les opcions, obtindreu més informació sobre el que "
"descarregareu."

#: ../../reference_manual/render_animation.rst:73
#: ../../reference_manual/render_animation.rst:93
msgid "For OSX"
msgstr "Per a OSX"

# skip-rule: t-acc_obe
#: ../../reference_manual/render_animation.rst:75
msgid ""
"Please see the section above. However, FFmpeg is obtained from `here "
"<https://evermeet.cx/ffmpeg/>`_ instead. Just pick the big green button on "
"the left under the FFmpeg heading. You will also need an archiving utility "
"that supports .7z, since FFmpeg provides their OSX builds in .7z format. If "
"you don't have one, try something like `Keka <https://www.kekaosx.com>`_."
msgstr ""
"Consulteu l'anterior secció. No obstant això, el FFmpeg s'obté des d'`aquí "
"<https://evermeet.cx/ffmpeg/>`_. Simplement seleccioneu el gran botó verd "
"que hi ha a l'esquerra sota de la capçalera FFmpeg. També necessitareu una "
"utilitat d'arxiu que admeti .7z, ja que el FFmpeg proporciona les seves "
"compilacions OSX en aquest format. Si no en teniu una, proveu amb quelcom "
"com el `Keka <https://www.kekaosx.com>`_."

#: ../../reference_manual/render_animation.rst:78
#: ../../reference_manual/render_animation.rst:98
msgid "For Linux"
msgstr "Per a Linux"

#: ../../reference_manual/render_animation.rst:80
msgid ""
"FFmpeg can be installed from the repositories on most Linux systems. Version "
"2.6 is required for proper gif support, as we use the palettegen "
"functionality."
msgstr ""
"El FFmpeg es pot instal·lar a partir dels repositoris en la majoria dels "
"sistemes Linux. La versió 2.6 és necessària per al suport adequat del GIF, "
"ja que utilitzem la funcionalitat «paletegen»."

#: ../../reference_manual/render_animation.rst:83
msgid "Step 2 - Unzipping and Linking to Krita"
msgstr "Pas 2: Descomprimir i vincular-lo amb el Krita"

#: ../../reference_manual/render_animation.rst:88
msgid ""
"Unzip the package that was just downloaded. Rename the long folder name to "
"just ffmpeg. Let's put this folder in a easy to find location. Go to your C:"
"\\ and place it there. You can put it wherever you want, but that is where I "
"put it."
msgstr ""
"Descomprimiu el paquet que acabeu de descarregar. Reanomeneu el llarg nom de "
"la carpeta a només «ffmpeg». Poseu aquesta carpeta en una ubicació fàcil de "
"trobar. Aneu a la vostra unitat C: i poseu-la allà. Podreu posar-la on "
"vulgueu, però aquí és on la poso jo."

# skip-rule: t-acc_obe
#: ../../reference_manual/render_animation.rst:90
msgid ""
"Open Krita back up and go to :menuselection:`File --> Render Animation`. "
"Click the :guilabel:`Browse` button on the last item called FFmpeg. Select "
"this file ``C:/ffmpeg/bin/ffmpeg.exe`` and click :guilabel:`OK`."
msgstr ""
"Obriu el Krita de nou i aneu a :menuselection:`Fitxer --> Renderitza "
"l'animació`. Feu clic al botó :guilabel:`Navega` i sobre l'últim element "
"anomenat FFmpeg. Seleccioneu aquest fitxer ``C:/ffmpeg/bin/ffmpeg.exe`` i "
"feu clic a :guilabel:`D'acord`."

# skip-rule: t-acc_obe
#: ../../reference_manual/render_animation.rst:95
msgid ""
"After downloading FFmpeg, you just need to extract it and then simply point "
"to it in the FFmpeg location in Krita like ``/Users/user/Downloads/ffmpeg`` "
"(assuming you downloaded and extracted the .7z file to /Users/user/"
"Downloads)."
msgstr ""
"Després de descarregar el FFmpeg, només necessitareu extreure'l i després "
"assenyalar la seva ubicació al Krita com ``/Users/usuari/Baixades/ffmpeg`` "
"(assumint que el vàreu descarregar i extreure a ``/Users/usuari/Baixades``)."

# skip-rule: t-acc_obe
#: ../../reference_manual/render_animation.rst:100
msgid ""
"FFmpeg is, if installed from the repositories, usually found in ``/usr/bin/"
"ffmpeg``."
msgstr ""
"El FFmpeg estarà, si s'instal·la des dels repositoris, generalment es "
"trobarà a ``/usr/bin/ffmpeg``."

#: ../../reference_manual/render_animation.rst:103
msgid "Step 3 - Testing out an animation"
msgstr "Pas 3: Provar una animació"

#: ../../reference_manual/render_animation.rst:105
msgid ""
"ffmpeg.exe is what Krita uses to do all of its animation export magic. Now "
"that it is hooked up, let us test it out."
msgstr ""
"El fitxer «ffmpeg.exe» és el que utilitzarà el Krita per a fer tota la seva "
"màgica exportació de l'animació. Ara que esteu preparat, provem-ho."

#: ../../reference_manual/render_animation.rst:107
msgid ""
"Let's make an animated GIF. In the Render Animation dialog, change the :"
"guilabel:`Render As`  field to \"GIF image\". Choose the file location where "
"it will save with the \"File\" menu below. I just saved it to my desktop and "
"called it \"export.gif\". When it is done, you should be able to open it up "
"and see the animation."
msgstr ""
"Crearem un GIF animat. En el diàleg Renderitza l'animació, canvieu el camp :"
"guilabel:`Renderitza` com a «Imatge GIF». Trieu la ubicació del fitxer on es "
"desarà amb el menú «Fitxer» a sota. Jo la vaig desar al meu escriptori i la "
"vaig anomenar «exportació.gif». En acabar, hauríeu de poder-la obrir i veure."

#: ../../reference_manual/render_animation.rst:111
msgid ""
"By default, FFmpeg will render MP4 files with a too new codec, which means "
"that Windows Media Player won't be able to play it. So for Windows, select "
"\"baseline\" for the profile instead of \"high422\" before rendering."
msgstr ""
"De manera predeterminada, el FFmpeg renderitzarà els fitxers MP4 amb un "
"còdec massa nou, el qual voldrà dir que el Windows Media Player no podrà "
"reproduir-lo. De manera que per a Windows, seleccioneu «línia base» per al "
"perfil en lloc de «high422» abans de renderitzar."

#: ../../reference_manual/render_animation.rst:115
msgid ""
"OSX does not come with any software to play MP4 and MKV files. If you use "
"Chrome for your web browser, you can drag the video file into that and the "
"video should play. Otherwise you will need to get a program like VLC to see "
"the video."
msgstr ""
"L'OSX no ve amb cap programari per a reproduir fitxers MP4 i MKV. Si "
"utilitzeu Chrome com al vostre navegador web, podreu arrossegar el fitxer de "
"vídeo a aquest i el vídeo s'hauria de reproduir. En cas contrari, "
"necessitareu obtenir un programa com VLC per a veure el vídeo."
