# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 23:01+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Over KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr "Het menu Help in Krita."

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr "Info over"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr "Handboek"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr "Bug"

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr "Menu Help"

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Handboek van Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr "Opent een browser en stuurt u naar de index van deze handleiding."

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Bug rapporteren"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr "Stuurt u naar de bugtracker."

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr "Toont systeeminformatie voor bugreports."

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""
"Dit is een selectie van alle moeilijk te ontdekken technische informatie van "
"uw computer. Dit omvat dingen zoals, welke versie van Krita hebt u, welke "
"versie heeft uw besturingssysteem, en het meest voorzichtig, welk soort van "
"OpenGL functionaliteit kan uw computer leveren. Dat laatste varieert heel "
"veel tussen computers en vanwege dat is het een van de mest moeilijke dingen "
"om te debuggen. Door het leveren van zulke informatie kan het ons helpen uit "
"te vinden wat de oorzaak van een bug is."

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "Over Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr "Toont u de dankbetuiging."

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr "Vertelt u iets over de KDE gemeenschap waar Krita onderdeel van is."
