# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:1
msgid "How to use filter layers in Krita."
msgstr "Hur man använder filterlager i Krita."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:19
msgid "Filter Layer"
msgstr "Filterlager"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:22
msgid ""
"Filter layers show whatever layers are underneath them, but with a filter "
"such as Layer Styles, Blur, Levels, Brightness / Contrast. For example, if "
"you add a **Filter Layer**, and choose the Blur filter, you will see every "
"layer under your filter layer blurred."
msgstr ""
"Filterlager visar vilket lager som än finns under dem, men med ett filter "
"såsom Lagerstilar, Suddighet, Nivåer, Ljusstyrka-kontrast. Om man exempelvis "
"lägger till ett **filterlager** och väljer suddighetsfiltret, ser alla lager "
"under filterlagret suddiga ut."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:24
msgid ""
"Unlike applying a filter directly on to a section of a Paint Layer, Filter "
"Layers do not actually alter the original image in the Paint Layers below "
"them. Once again, non-destructive editing! You can tweak the filter at any "
"time, and the changes can always be altered or removed."
msgstr ""
"I motsats till att använda ett filter direkt på en del av ett målarlager, "
"ändrar filterlager faktiskt inte originalbilden på målarlagren under dem. "
"Återigen, icke-destruktiv redigering! Filtret kan justeras när som helst, "
"och ändringarna kan alltid modifieras eller tas bort."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:26
msgid ""
"Unlike Filter Masks though, Filter Layers apply to the entire canvas for the "
"layers beneath. If you wish to apply a filter layer to only *some* layers, "
"then you can utilize the Group Layer feature and add those layers into a "
"group with the filter layer on top of the stack."
msgstr ""
"I motsats till filtermasker, gäller dock filterlager underliggande lager på "
"hela duken. Om man vill att ett filterlager bara ska gälla *vissa* lager, "
"kan grupplagerfunktionen användas, och lagren kan läggas till  i en grupp "
"med filterlagret överst."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:28
msgid ""
"You can edit the settings for a filter layer, by double clicking on it in "
"the Layers docker."
msgstr ""
"Inställningarna för ett filterlager kan redigeras genom att dubbelklicka på "
"det i lagerpanelen."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:31
msgid ""
"Only Krita native filters (the ones in the :guilabel:`Filters` menu) can be "
"used with Filter Layers. Filter Layers are not supported using the "
"externally integrated G'Mic filters."
msgstr ""
"Bara filter inbyggda i Krita (de som finns i menyn :guilabel:`Filter`) kan "
"användas med filterlager. Filterlager stöds inte vid användning av de "
"externa integrerade G'Mic-filtren."
