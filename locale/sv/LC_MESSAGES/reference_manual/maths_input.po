# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-25 17:15+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/maths_input.rst:1
msgid ""
"Overview of maths operations that can be used in Krita spinboxes and number "
"inputs."
msgstr ""
"Översikt av matematiska operationer som kan användas i Kritas nummerrutor "
"och nummerinmatning."

#: ../../reference_manual/maths_input.rst:12
msgid "Maths"
msgstr "Matematik"

#: ../../reference_manual/maths_input.rst:17
msgid "Maths Input"
msgstr "Matematikinmatning"

#: ../../reference_manual/maths_input.rst:19
msgid ""
"Also known as Numerical Input boxes. You can make Krita do simple maths for "
"you in the places where we have number input. Just select the number in a "
"spinbox, or right-click a slider to activate number input. It doesn't do "
"unit conversion yet, but this is planned."
msgstr ""
"Kallas också numeriska inmatningsrutor. Man kan få Krita att utföra enkel "
"matematik på platserna där vi har numerisk inmatning. Markera bara talet i "
"en nummerruta, eller högerklicka på ett skjutreglage för att aktivera "
"nummerinmatning. De gör inte enhetskonvertering ännu, men det är planerat."

#: ../../reference_manual/maths_input.rst:22
msgid "Possible Functions"
msgstr "Tillgängliga funktioner"

#: ../../reference_manual/maths_input.rst:25
msgid "Just adds the numbers. Usage: ``50+100`` Output: ``150``"
msgstr "Adderar bara talen. Användning: ``50+100`` Resultat: ``150``"

#: ../../reference_manual/maths_input.rst:26
msgid "Addition (Operator: + )"
msgstr "Addition (operator: + )"

#: ../../reference_manual/maths_input.rst:29
msgid ""
"Just subtracts the last number from the first. Usage: ``50-100`` Output: "
"``50``"
msgstr ""
"Subtraherar bara det sista talet från det första. Användning: ``50-100`` "
"Resultat: ``50``"

#: ../../reference_manual/maths_input.rst:30
msgid "Subtraction (Operator: - )"
msgstr "Subtraktion (operator: - )"

#: ../../reference_manual/maths_input.rst:33
msgid "Just multiplies the numbers. Usage: ``50*100`` Output: ``5000``"
msgstr "Multiplicerar bara talen. Användning: ``50*100`` Resultat: ``5000``"

#: ../../reference_manual/maths_input.rst:34
msgid "Multiplication (Operator: * )"
msgstr "Multiplikation (operator: * )"

#: ../../reference_manual/maths_input.rst:37
msgid "Just divides the numbers. Usage: ``50/100`` Output: ``0.5``"
msgstr "Dividerar bara talen. Användning: ``50/100`` Output: ``0,5``"

#: ../../reference_manual/maths_input.rst:38
msgid "Division (Operator: / )"
msgstr "Division (operator: / )"

#: ../../reference_manual/maths_input.rst:41
msgid ""
"Makes the last number the exponent of the first and calculates the result. "
"Usage: ``2^8`` Output: ``256``"
msgstr ""
"Gör det sista talet till exponenten av det första och beräknar resultatet. "
"Användning: ``2^8`` Resultat: ``256``"

#: ../../reference_manual/maths_input.rst:42
msgid "Exponent (Operator: ^ )"
msgstr "Exponent (operator: ^ )"

#: ../../reference_manual/maths_input.rst:45
msgid ""
"Gives you the sine of the given angle. Usage: ``sin(50)`` Output: ``0.76``"
msgstr ""
"Ger sinus av den angivna vinkeln. Användning: ``sin(50)`` Resultat: ``0.76``"

#: ../../reference_manual/maths_input.rst:46
msgid "Sine (Operator: sin() )"
msgstr "Sinus (operator: sin() )"

#: ../../reference_manual/maths_input.rst:49
msgid ""
"Gives you the cosine of the given angle. Usage: ``cos(50)`` Output: ``0.64``"
msgstr ""
"Ger cosinus av den angivna vinkeln. Användning: ``cos(50)`` Resultat: "
"``0,64``"

#: ../../reference_manual/maths_input.rst:50
msgid "Cosine (Operator: cos() )"
msgstr "Cosinus (operator: cos() )"

#: ../../reference_manual/maths_input.rst:53
msgid ""
"Gives you the tangent of the given angle. Usage: ``tan(50)`` Output: ``1.19``"
msgstr ""
"Ger tangens av den angivna vinkeln. Användning: ``tan(50)`` Resultat: "
"``1,19``"

#: ../../reference_manual/maths_input.rst:54
msgid "Tangent (Operator: tan() )"
msgstr "Tangens (operator: tan() )"

#: ../../reference_manual/maths_input.rst:57
msgid ""
"Inverse function of the sine, gives you the angle which the sine equals the "
"argument. Usage: ``asin(0.76)`` Output: ``50``"
msgstr ""
"Invers funktion av sinus, ger vinkeln då sinus är lika med argumentet. "
"Användning: ``asin(0,76)`` Resultat: ``50``"

#: ../../reference_manual/maths_input.rst:58
msgid "Arc Sine (Operator: asin() )"
msgstr "Arcus sinus (operator: asin() )"

#: ../../reference_manual/maths_input.rst:61
msgid ""
"Inverse function of the cosine, gives you the angle which the cosine equals "
"the argument. Usage: ``acos(0.64)`` Output: ``50``"
msgstr ""
"Invers funktion av cosinus, ger vinkeln då cosinus är lika med argumentet. "
"Användning: ``acos(0,64)`` Resultat: ``50``"

#: ../../reference_manual/maths_input.rst:62
msgid "Arc Cosine (Operator: acos() )"
msgstr "Arcus cosinus (operator: acos() )"

#: ../../reference_manual/maths_input.rst:65
msgid ""
"Inverse function of the tangent, gives you the angle which the tangent "
"equals the argument. Usage: ``atan(1.19)`` Output: ``50``"
msgstr ""
"Invers funktion av tangens, ger vinkeln då tangens är lika med argumentet. "
"Användning: ``atan(1,19)`` Resultat: ``50``"

#: ../../reference_manual/maths_input.rst:66
msgid "Arc Tangent (Operator: atan() )"
msgstr "Arcus tangens (operator: atan() )"

#: ../../reference_manual/maths_input.rst:69
msgid ""
"Gives you the value without negatives. Usage: ``abs(75-100)`` Output: ``25``"
msgstr ""
"Ger värdet utan minustecken. Användning: ``abs(75-100)`` Resultat: ``25``"

#: ../../reference_manual/maths_input.rst:70
msgid "Absolute (Operator: abs() )"
msgstr "Absolutvärde (operator: abs() )"

#: ../../reference_manual/maths_input.rst:73
msgid ""
"Gives you given values using e as the exponent. Usage: ``exp(1)`` Output: "
"``2.7183``"
msgstr ""
"Ger angivet värde med användning av e som exponent. Användning:  ``exp(1)`` "
"Resultat: ``2,7183``"

#: ../../reference_manual/maths_input.rst:74
msgid "Exponent (Operator: exp() )"
msgstr "Exponent (operator: exp() )"

#: ../../reference_manual/maths_input.rst:77
msgid ""
"Gives you the natural logarithm, which means it has the inverse "
"functionality to exp(). Usage: ``ln(2)`` Output: ``0.6931``"
msgstr ""
"Ger den naturliga logaritmen, vilket betyder att den har invers effekt av "
"exp(). Användning: ``ln(2)`` Resultat: ``0,6931``"

#: ../../reference_manual/maths_input.rst:79
msgid "Natural Logarithm (Operator: ln() )"
msgstr "Naturlig logaritm (operator: ln() )"

#: ../../reference_manual/maths_input.rst:81
msgid "The following are technically supported but bugged:"
msgstr "De följande stöds tekniskt sett, men är felaktiga:"

#: ../../reference_manual/maths_input.rst:84
msgid ""
"Gives you logarithms of the given value. Usage: ``log10(50)`` Output: "
"``0.64``"
msgstr ""
"Ger logaritmen av angivet värde. Användning: ``log10(50)`` Resultat: ``0,64``"

#: ../../reference_manual/maths_input.rst:86
msgid "Common Logarithm (Operator: log10() )"
msgstr "Tiologaritm (operator: log10() )"

#: ../../reference_manual/maths_input.rst:89
msgid "Order of Operations."
msgstr "Operationernas ordning."

#: ../../reference_manual/maths_input.rst:91
msgid ""
"The order of operations is a globally agreed upon reading order for "
"interpreting mathematical expressions. It solves how to read an expression "
"like:"
msgstr ""
"Operationernas ordning är den allmänt vedertagna ordningen för att tolka "
"matematiska uttryck. Den löser hur man läser ett uttryck som:"

#: ../../reference_manual/maths_input.rst:93
msgid "``2+3*4``"
msgstr "\"2+3*4\""

#: ../../reference_manual/maths_input.rst:95
msgid ""
"You could read it as 2+3 = 5, and then 5*4 =20. Or you could say 3*4 = 12 "
"and then 2+12 = 14."
msgstr ""
"Man skulle kunna läsa det som 2 + 3 = 5, och sedan 5*4 = 20, Eller man "
"skulle kunna mena 3*4 = 12 och sedan 2+12 = 14."

#: ../../reference_manual/maths_input.rst:97
msgid ""
"The order of operations itself is Exponents, Multiplication, Addition, "
"Subtraction. So we first multiply, and then add, making the answer to the "
"above 14, and this is how Krita will interpret the above."
msgstr ""
"Själva operationernas ordning är exponenter, multiplikation, addition, "
"subtraktion. Så vi multiplicerar först och adderar sedan, vilket gör att "
"svaret för ovanstående är 14, och det är så Krita tolkar det."

#: ../../reference_manual/maths_input.rst:99
msgid ""
"We can use brackets to specify certain operations go first, so to get 20 "
"from the above expression, we do the following:"
msgstr ""
"Man kan använda parenteser för att ange att vissa operationer ska utföras "
"först, så för att få 20 från uttrycket ovan, gör vi följande:"

#: ../../reference_manual/maths_input.rst:101
msgid "``( 2+3 )*4``"
msgstr "\"( 2+3 )*4\""

#: ../../reference_manual/maths_input.rst:103
msgid ""
"Krita can interpret the brackets accordingly and will give 20 from this."
msgstr "Krita kan tolka parenteserna enligt det, och ger 20 som resultat."

#: ../../reference_manual/maths_input.rst:106
msgid "Errors"
msgstr "Fel"

#: ../../reference_manual/maths_input.rst:108
msgid ""
"Sometimes, you see the result becoming red. This means you made a mistake "
"and Krita cannot parse your maths expression. Simply click the input box and "
"try again."
msgstr ""
"Ibland ser man att resultatet blir rött. Det betyder att man har gjort ett "
"misstag, och att Krita inte kan tolka det matematiska uttrycket. Klicka helt "
"enkelt på inmatningsrutan och försök igen."
