# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:26+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:84
msgid ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"
msgstr ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: referensverktyg"

#: ../../reference_manual/tools/reference_images_tool.rst:1
msgid "The reference images tool"
msgstr "Referensbildverktyget"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Reference"
msgstr "Referens"

#: ../../reference_manual/tools/reference_images_tool.rst:15
msgid "Reference Images Tool"
msgstr "Referensbildverktyg"

#: ../../reference_manual/tools/reference_images_tool.rst:17
msgid "|toolreference|"
msgstr "|toolreference|"

#: ../../reference_manual/tools/reference_images_tool.rst:21
msgid ""
"The reference images tool is a replacement for the reference images docker. "
"You can use it to load images from your disk as reference, which can then be "
"moved around freely on the canvas and placed wherever."
msgstr ""
"Referensbildverktyget är en ersättning för referensbildpanelen. Det kan "
"användas för att läsa in bilder från disk som referens, vilken sedan fritt "
"kan flyttas omkring på duken och placeras var som helst."

#: ../../reference_manual/tools/reference_images_tool.rst:24
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/reference_images_tool.rst:26
msgid "Add reference image"
msgstr "Lägg till referensbild"

#: ../../reference_manual/tools/reference_images_tool.rst:27
msgid "Load a single image to display on the canvas."
msgstr "Läs in en enskild bild att visa på duken."

#: ../../reference_manual/tools/reference_images_tool.rst:28
msgid "Load Set"
msgstr "Läs in uppsättning"

#: ../../reference_manual/tools/reference_images_tool.rst:29
msgid "Load a set of reference images."
msgstr "Läser in en uppsättning referensbilder."

#: ../../reference_manual/tools/reference_images_tool.rst:30
msgid "Save Set"
msgstr "Spara uppsättning"

#: ../../reference_manual/tools/reference_images_tool.rst:31
msgid "Save a set of reference images."
msgstr "Sparar en uppsättning referensbilder."

#: ../../reference_manual/tools/reference_images_tool.rst:32
msgid "Delete all reference images"
msgstr "Ta bort alla referensbilder"

#: ../../reference_manual/tools/reference_images_tool.rst:33
msgid "Delete all the reference images"
msgstr "Ta bort alla referensbilder"

#: ../../reference_manual/tools/reference_images_tool.rst:34
msgid "Keep aspect ratio"
msgstr "Behåll proportion"

#: ../../reference_manual/tools/reference_images_tool.rst:35
msgid "When toggled this will force the image to not get distorted."
msgstr "När det ändras tvingar det att bilden inte blir förvrängd."

#: ../../reference_manual/tools/reference_images_tool.rst:36
msgid "Opacity"
msgstr "Ogenomskinlighet"

#: ../../reference_manual/tools/reference_images_tool.rst:37
msgid "Lower the opacity."
msgstr "Sänker ogenomskinligheten."

#: ../../reference_manual/tools/reference_images_tool.rst:38
msgid "Saturation"
msgstr "Färgmättnad"

#: ../../reference_manual/tools/reference_images_tool.rst:39
msgid ""
"Desaturate the image. This is useful if you only want to focus on the light/"
"shadow instead of getting distracted by the colors."
msgstr ""
"Avmätta bilden. Det är användbart om man bara vill fokusera på ljus och "
"skugga istället för att bli distraherad av färgerna."

#: ../../reference_manual/tools/reference_images_tool.rst:41
msgid "How is the reference image stored."
msgstr "Hur referensbilden lagras."

#: ../../reference_manual/tools/reference_images_tool.rst:43
msgid "Embed to \\*.kra"
msgstr "Inbädda i \\.kra"

#: ../../reference_manual/tools/reference_images_tool.rst:44
msgid ""
"Store this reference image into the kra file. This is recommended for small "
"vital files you'd easily lose track of otherwise."
msgstr ""
"Lagra referensbilden i kra-filen. Det rekommenderas för små virtuella filer "
"som man annars kan tappa bort."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Storage mode"
msgstr "Lagringsmetod"

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Link to external file."
msgstr "Länk till extern fil."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid ""
"Only link to the reference image, krita will open it from the disk everytime "
"it loads this file. This is recommended for big files, or files that change "
"a lot."
msgstr ""
"Länka bara till referensbilden. Krita öppnar den från disk varje gång filen "
"läses in. Det rekommenderas för stora filer, eller filer om ofta ändras."

#: ../../reference_manual/tools/reference_images_tool.rst:48
msgid ""
"You can move around reference images by selecting them with |mouseleft|, and "
"dragging them. You can rotate reference images by holding the cursor close "
"to the outside of the corners till the rotate cursor appears, while tilting "
"is done by holding the cursor close to the outside of the middle nodes. "
"Resizing can be done by dragging the nodes. You can delete a single "
"reference image by clicking it and pressing the :kbd:`Del` key. You can "
"select multiple reference images with the :kbd:`Shift` key and perform all "
"of these actions."
msgstr ""
"Man kan flytta omkring referensbilder genom att markera dem med vänster "
"musknapp och dra dem. Man kan rotera referensbilder genom att hålla markören "
"nära hörnens utsida tills rotationsmarkören visas, medan lutning görs genom "
"att hålla markören nära utsidan av mittnoderna. Storleksändring kan göras "
"genom att dra noderna. Man kan ta bort en enskild referensbild genom att "
"klicka på den och trycka på tangenten :kbd:`Del`. Man kan markera flera "
"referensbilder med tangenten :kbd:`Skift` och utföra alla de här åtgärderna."

#: ../../reference_manual/tools/reference_images_tool.rst:50
msgid ""
"To hide all reference images temporarily use :menuselection:`View --> Show "
"Reference Images`."
msgstr ""
"Använd :menuselection:`Visa --> Visa referensbilder`. för att tillfälligt "
"dölja alla referensbilder."
