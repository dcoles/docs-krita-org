# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:15+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Komma igång"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""
"Välkommen till Kritas handbok! I det här avsnittet kommer vi att försöka "
"hjälpa dig komma igång."

#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""
"Om du är bekant med digitalmålning, rekommenderar vi att ta en titt på "
"kategorin :ref:`introduction_from_other_software`, som innehåller "
"handledningar som hjälper dig bli bekant med Krita genom att jämföra dess "
"funktioner med annan programvara."

#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"Om du är ny på digitalkonst, börja med :ref:`installation`, som behandlar "
"installation av Krita, och fortsätt med :ref:`starting_with_krita`, som "
"hjälper dig skapa ett nytt dokument och spara det, :ref:`basic_concepts`, "
"där vi snabbt försöker täcka de stora kategorierna av funktionalitet i "
"Krita, och till sist :ref:`navigation`, som hjälper dig att hitta hjälp om "
"grundläggande användning, såsom panorering, zoomning och rotation."

#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"När du har bemästrat det, kan du ta en titt i de speciella "
"introduktionssidorna till funktionalitet i :ref:`user_manual`, läsa igenom "
"de övergripande begreppen bakom (digital) målning i avsnittet :ref:"
"`general_concepts`, eller bara söka i :ref:`reference_manual` för att hitta "
"vad en specifik knapp gör."

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Innehåll:"
