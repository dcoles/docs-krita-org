# Translation of docs_krita_org_general_concepts___projection___axonometric.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___projection___axonometric\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:40+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_07.svg"
msgstr ".. image:: images/category_projection/projection-cube_07.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_08.svg"
msgstr ".. image:: images/category_projection/projection-cube_08.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_10.svg"
msgstr ".. image:: images/category_projection/projection-cube_10.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection-cube_11.svg"
msgstr ".. image:: images/category_projection/projection-cube_11.svg"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_15.png"
msgstr ".. image:: images/category_projection/projection_image_15.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_16.png"
msgstr ".. image:: images/category_projection/projection_image_16.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_17.png"
msgstr ".. image:: images/category_projection/projection_image_17.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_18.png"
msgstr ".. image:: images/category_projection/projection_image_18.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_19.png"
msgstr ".. image:: images/category_projection/projection_image_19.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_20.png"
msgstr ".. image:: images/category_projection/projection_image_20.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_21.png"
msgstr ".. image:: images/category_projection/projection_image_21.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_22.png"
msgstr ".. image:: images/category_projection/projection_image_22.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_23.png"
msgstr ".. image:: images/category_projection/projection_image_23.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_24.png"
msgstr ".. image:: images/category_projection/projection_image_24.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_25.png"
msgstr ".. image:: images/category_projection/projection_image_25.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_26.png"
msgstr ".. image:: images/category_projection/projection_image_26.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_27.png"
msgstr ".. image:: images/category_projection/projection_image_27.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_28.png"
msgstr ".. image:: images/category_projection/projection_image_28.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_29.png"
msgstr ".. image:: images/category_projection/projection_image_29.png"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_animation_02.gif"
msgstr ".. image:: images/category_projection/projection_animation_02.gif"

#: ../../general_concepts/projection/axonometric.rst:None
msgid ".. image:: images/category_projection/projection_image_30.png"
msgstr ".. image:: images/category_projection/projection_image_30.png"

#: ../../general_concepts/projection/axonometric.rst:1
msgid "Axonometric projection."
msgstr "Аксонометрична проєкція."

#: ../../general_concepts/projection/axonometric.rst:10
msgid ""
"This is a continuation of :ref:`the orthographic and oblique tutorial "
"<projection_orthographic>`, be sure to check it out if you get confused!"
msgstr ""
"Це продовження сторінки щодо :ref:`ортографічної та похилої проєкцій "
"<projection_orthographic>`. Щоб уникнути непорозумінь, спочатку ознайомтеся "
"із попередніми сторінками!"

#: ../../general_concepts/projection/axonometric.rst:12
#: ../../general_concepts/projection/axonometric.rst:16
msgid "Axonometric"
msgstr "Аксонометрична проєкція"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Projection"
msgstr "Проєкція"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Dimetric"
msgstr "Диметрична проєкція"

#: ../../general_concepts/projection/axonometric.rst:12
msgid "Isometric"
msgstr "Ізометрична проєкція"

#: ../../general_concepts/projection/axonometric.rst:18
msgid "So, the logic of adding the top is still similar to that of the side."
msgstr ""
"Отже, логіка додавання виду згори є подібною до логіки додавання виду збоку."

#: ../../general_concepts/projection/axonometric.rst:23
msgid ""
"Not very interesting. But it gets much more interesting when we use a side "
"projection:"
msgstr ""
"Не дуже цікаво. Але усе стає набагато цікавішим, якщо ми скористаємося "
"бічною проєкцією:"

#: ../../general_concepts/projection/axonometric.rst:28
msgid ""
"Because our cube is red on both front-sides, and blue on both left and right "
"side, we can just use copies, this simplifies the method for cubes a lot. We "
"call this form of axonometric projection 'dimetric' as it deforms two "
"parallel lines equally."
msgstr ""
"Оскільки наш паралелепіпед є червоним з обох передніх боків і синім з лівого "
"і правого боків, ми можемо просто скористатися копіями. Це значно спрощує "
"наш метод для паралелепіпедів. Ми називаємо цю форму аксонометричної "
"проєкції «диметрією» оскільки вона деформує однаково дві паралельні лінії."

#: ../../general_concepts/projection/axonometric.rst:30
msgid ""
"Isometric is sorta like dimetric where we have the same angle between all "
"main lines:"
msgstr ""
"Ізометрія є подібною до диметрії, де ми маємо один кут між усіма основними "
"лініями:"

#: ../../general_concepts/projection/axonometric.rst:35
msgid ""
"True isometric is done with a 90-54.736=35.264° angle from ground plane:"
msgstr ""
"Істина ізометрія утворюється кутом 90-54.736=35.264° від площини основи:"

#: ../../general_concepts/projection/axonometric.rst:40
msgid ""
"(as you can see, it doesn't line up perfectly, because Inkscape, while more "
"designed for making these kinds of diagrams than Krita, doesn't have tools "
"to manipulate the line's angle in degrees)"
msgstr ""
"(як можна бачити, вирівнювання не є ідеальним, оскільки в Inkscape, хоча ця "
"програма і є придатнішою для створення такого типу діаграм, ніж Krita, немає "
"інструментів для визначення кута відхилення ліній у градусах)"

#: ../../general_concepts/projection/axonometric.rst:42
msgid ""
"This is a bit of an awkward angle, and on top of that, it doesn't line up "
"with pixels sensibly, so for videogames an angle of 30° from the ground "
"plane is used."
msgstr ""
"Це дещо незручний кут і, окрім того, він не дуже добре вирівнюється за "
"пікселями. Тому у відеоіграх використовують кут 30° від площини основи."

#: ../../general_concepts/projection/axonometric.rst:47
msgid "Alright, so, let's make an isometric out of our boy then."
msgstr "Гаразд, тепер створимо ізометрію з нашого зображення голови хлопчика."

#: ../../general_concepts/projection/axonometric.rst:49
msgid "We make a new document, and add a vector layer."
msgstr "Ми створимо документ і додамо векторний шар."

#: ../../general_concepts/projection/axonometric.rst:51
msgid ""
"On the vector layer, we select the straight line tool, start a line and then "
"hold the :kbd:`Shift` key to make it snap to angles. This'll allow us to "
"make a 30° setup like above:"
msgstr ""
"На векторному шарі ми виберемо інструмент малювання прямої, розпочнемо "
"малювати пряму, а потім натиснемо клавішу :kbd:`Shift` для прилипання лінії "
"до кутів. Це надасть нам змогу створювати лінії під кутом 30°, як у прикладі "
"вище:"

#: ../../general_concepts/projection/axonometric.rst:56
msgid ""
"We then import some of the frames from the animation via :menuselection:"
"`Layers --> Import/Export --> Import layer`."
msgstr ""
"Далі, імпортуємо деякі кадри з анімації за допомогою пункту меню :"
"menuselection:`Шари --> Імпортування/Експортування --> Імпортувати шар`."

#: ../../general_concepts/projection/axonometric.rst:58
msgid ""
"Then crop it by setting the crop tool to :guilabel:`Layer`, and use :"
"menuselection:`Filters --> Colors --> Color to alpha` to remove any "
"background. I also set the layers to 50% opacity. We then align the vectors "
"to them:"
msgstr ""
"Потім обріжемо його встановленням для інструмента обрізання значення :"
"guilabel:`Шар` і вибором пункту меню :menuselection:`Фільтри --> Кольори --> "
"Колір до альфи` для вилучення тла. Також автор встановив для шарів 50% "
"непрозорості. Потім вирівняв вектори за головами:"

#: ../../general_concepts/projection/axonometric.rst:65
msgid ""
"To resize a vector but keep its angle, you just select it with the shape "
"handling tool (the white arrow) drag on the corners of the bounding box to "
"start moving them, and then press the :kbd:`Shift` key to constrain the "
"ratio. This'll allow you to keep the angle."
msgstr ""
"Щоб змірити розміри вектора, але зберегти його напрямок, ви можете позначити "
"його за допомогою вказівника інструмента роботи з формами (білої стрілочки), "
"почати перетягувати один з кутів обмежувальної рамки вектора, а потім "
"натиснути клавішу :kbd:`Shift`, щоб зафіксувати співвідношення розмірів "
"форми. Це надасть вам змогу не змінювати кут напрямку вектора."

#: ../../general_concepts/projection/axonometric.rst:67
msgid ""
"The lower image is 'the back seen from the front', we'll be using this to "
"determine where the ear should go."
msgstr ""
"На нижчому зображенні показано «вид на задню частину спереду». Ми "
"скористаємося ним для визначення місця для вуха."

#: ../../general_concepts/projection/axonometric.rst:69
msgid ""
"Now, we obviously have too little space, so select the crop tool, select :"
"guilabel:`Image` and tick :guilabel:`Grow` and do the following:"
msgstr ""
"Далі, у нас, очевидно, замало місця. Тому виберемо інструмент обрізання, "
"виберемо :guilabel:`Зображення`, позначимо пункт :guilabel:`Збільшити` і "
"зробимо так:"

#: ../../general_concepts/projection/axonometric.rst:74
msgid ""
"Grow is a more practical way of resizing the canvas in width and height "
"immediately."
msgstr ""
"Збільшення є практичнішим способом негайної зміни розмірів полотна за "
"шириною і висотою."

#: ../../general_concepts/projection/axonometric.rst:76
msgid ""
"Then we align the other heads and transform them by using the transform tool "
"options:"
msgstr ""
"Далі, вирівняємо інші голови і перетворимо їх за допомогою параметрів "
"інструмента перетворення:"

#: ../../general_concepts/projection/axonometric.rst:81
msgid "(330° here is 360°-30°)"
msgstr "(330° тут це 360°-30°)"

#: ../../general_concepts/projection/axonometric.rst:83
msgid ""
"Our rectangle we'll be working in slowly becomes visible. Now, this is a bit "
"of a difficult angle to work at, so we go to :menuselection:`Image --> "
"Rotate --> Rotate Image` and fill in 30° clockwise:"
msgstr ""
"Наш прямокутник, над яким ми працюватимемо, повільно стає видимим. Оскільки "
"його розташовано під незручним для роботи кутом, ми повернемо його за "
"допомогою пункту меню :menuselection:`Зображення --> Обернути --> Обернути "
"зображення`, вказавши обертання у 30° за годинниковою стрілкою:"

#: ../../general_concepts/projection/axonometric.rst:90
msgid ""
"(of course, we could've just rotated the left two images 30°, this is mostly "
"to be less confusing compared to the cube)"
msgstr ""
"(звичайно ж, ми могли б просто повернути два зображення ліворуч на 30°, це, "
"здебільшого, простіше за роботу з паралелепіпедом)"

#: ../../general_concepts/projection/axonometric.rst:92
msgid ""
"So, we do some cropping, some cleanup and add two parallel assistants like "
"we did with the orthographic:"
msgstr ""
"Отже, ми виконали певне обрізання, чищення і додали дві допоміжні паралельні "
"лінії, як це було раніше зроблено для ортографічної проєкції:"

#: ../../general_concepts/projection/axonometric.rst:97
msgid ""
"So the idea here is that you draw parallel lines from both sides to find "
"points in the drawing area. You can use the previews of the assistants for "
"this to keep things clean, but I drew the lines anyway for your convenience."
msgstr ""
"Отже, ідея полягає у малюванні паралельних ліній з обох боків для пошуку "
"точок на ділянці малювання. Ви можете скористатися просто попередніми "
"переглядами допоміжних ліній, щоб не захаращувати зображення зайвими "
"лініями, але автор намалював ці лінії, щоб зручніше було зрозуміти принципи "
"роботи."

#: ../../general_concepts/projection/axonometric.rst:102
msgid ""
"The best is to make a few sampling points, like with the eyebrows here, and "
"then draw the eyebrow over it."
msgstr ""
"Найкраще створити декілька точок-зразків, як ми це зробили у нашому прикладі "
"для брів, а потім намалювати за ними брови."

#: ../../general_concepts/projection/axonometric.rst:108
msgid "Alternative axonometric with the transform tool"
msgstr "Альтернативна аксонометрія із інструментом перетворення"

#: ../../general_concepts/projection/axonometric.rst:110
msgid ""
"Now, there's an alternative way of getting there that doesn't require as "
"much space."
msgstr ""
"Тепер скористаємося альтернативним способом отримання зображення, який не "
"потребує багато місця."

#: ../../general_concepts/projection/axonometric.rst:112
msgid ""
"We open our orthographic with :guilabel:`Open existing Document as Untitled "
"Document` so that we don't save over it."
msgstr ""
"Ми відкриємо наші ортографічні проєкції за допомогою пункту меню :guilabel:"
"`Відкрити наявний документ як документ без назви`, щоб не зберігати зміни до "
"початкового варіанта."

#: ../../general_concepts/projection/axonometric.rst:114
msgid ""
"Our game-safe isometric has its angle at two pixels horizontal is one pixel "
"vertical. So, we shear the ortho graphics with transform masks to -.5/+.5 "
"pixels (this is proportional)"
msgstr ""
"Наша призначена для відеоігор ізометрія має вісі, які розташовано так, що "
"двом пікселям за горизонталлю відповідає один піксель за вертикаллю. Отже, "
"ми перекосимо графіку за допомогою масок перетворення до -.5/+.5 пікселів "
"(пропорційно)."

#: ../../general_concepts/projection/axonometric.rst:119
msgid ""
"Use the grid to setup two parallel rulers that represent both diagonals (you "
"can snap them with the :kbd:`Shift + S` shortcut):"
msgstr ""
"Скористаймося ґраткою для додавання двох паралельних лінійок, які "
"відповідають обом діагоналями (ви можете увімкнути для них прилипання за "
"допомогою комбінації клавіш :kbd:`Shift + S`):"

#: ../../general_concepts/projection/axonometric.rst:124
msgid "Add the top view as well:"
msgstr "Додамо також вид згори:"

#: ../../general_concepts/projection/axonometric.rst:129
msgid "if you do this for all slices, you get something like this:"
msgstr "якщо ви зробили це для усіх перерізів, у вас вийде щось таке:"

#: ../../general_concepts/projection/axonometric.rst:134
msgid ""
"Using the parallel rulers, you can then figure out the position of a point "
"in 3d-ish space:"
msgstr ""
"Далі, за допомогою паралельних лінійок ви можете визначити розташування "
"точки у просторі:"

#: ../../general_concepts/projection/axonometric.rst:139
msgid "As you can see, this version both looks more 3d as well as more creepy."
msgstr ""
"Як бачите, ця версія одночасно виглядає більш просторовою і страхітливою."

#: ../../general_concepts/projection/axonometric.rst:141
msgid ""
"That's because there are less steps involved as the previous version -- "
"We're deriving our image directly from the orthographic view -- so there are "
"less errors involved."
msgstr ""
"Причиною є те, що ми виконали менше кроків, порівняно із попередньою "
"версією. Ми створили наше зображення безпосередньо на основі ортографічних "
"проєкцій, отже зменшили похибку."

#: ../../general_concepts/projection/axonometric.rst:143
msgid ""
"The creepiness is because we've had the tiniest bit of stylisation in our "
"side view, so the eyes come out HUGE. This is because when we stylize the "
"side view of an eye, we tend to draw it not perfectly from the side, but "
"rather slightly at an angle. If you look carefully at the turntable, the "
"same problem crops up there as well."
msgstr ""
"Певна страхітливість є наслідком того, що ми використали дещо стилізований "
"вид збоку, тому очі вийшли ВЕЛИЧЕЗНИМИ. Це тому, що стилізуючи вид збоку ока "
"ми намагалися намалювати його не зовсім збоку, а під певним кутом. Якщо ви "
"поглянете уважніше на обертальний круг, ви зможете помітити там ту саму "
"проблему."

#: ../../general_concepts/projection/axonometric.rst:145
msgid ""
"Generally, stylized stuff tends to fall apart in 3d view, and you might need "
"to make some choices on how to make it work."
msgstr ""
"Загалом кажучи, стилізація псує просторові форми. Ймовірно, вам доведеться "
"робити певний вибір щодо її використання, щоб мати змогу користуватися "
"проєкціями."

#: ../../general_concepts/projection/axonometric.rst:147
msgid ""
"For example, we can just easily fix the side view (because we used transform "
"masks, this is easy.)"
msgstr ""
"Наприклад, ми можемо просто виправити вид збоку (це просто, оскільки ми "
"використовували маски перетворення)."

#: ../../general_concepts/projection/axonometric.rst:152
msgid "And then generate a new drawing from that…"
msgstr "А потім створити з цього новий малюнок…"

#: ../../general_concepts/projection/axonometric.rst:157
msgid ""
"Compare to the old one and you should be able to see that the new result’s "
"eyes are much less creepy:"
msgstr ""
"Порівнюючи попередній результат із цим, маємо зауважити, що очі у нового "
"результату не такі страхітливі:"

#: ../../general_concepts/projection/axonometric.rst:162
msgid ""
"It still feels very squashed compared to the regular parallel projection "
"above, and it might be an idea to not just skew but also stretch the orthos "
"a bit."
msgstr ""
"Результат виглядає дещо стиснутим, порівняно із результатом звичайної "
"паралельної проєкції, наведеним вище. Тому варто не лише перекосити "
"ортографічні проєкції, а і дещо розтягнути їх."

#: ../../general_concepts/projection/axonometric.rst:164
msgid "Let's continue with perspective projection in the next one!"
msgstr ""
"У наступному розділі ми продовжимо вправлятися зі проєкцією перспективи!"

#~ msgid ""
#~ "to remove any background. I also set the layers to 50% opacity. We then "
#~ "align the vectors to them:"
#~ msgstr ""
#~ "для вилучення тла. Також автор встановив для шарів 50% непрозорості. "
#~ "Потім вирівняв вектори за головами:"
