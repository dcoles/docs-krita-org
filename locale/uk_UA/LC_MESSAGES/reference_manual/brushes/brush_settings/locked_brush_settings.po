# Translation of docs_krita_org_reference_manual___brushes___brush_settings___locked_brush_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___locked_brush_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:39+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Unlock (Keep Locked)"
msgstr "Розблокувати (Тримати заблокованим)"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:1
msgid "How to keep brush settings locked in Krita."
msgstr "Як заблокувати параметри пензля у Krita."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:11
#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:16
msgid "Locked Brush Settings"
msgstr "Заблоковані параметри пензля"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:19
msgid ""
"Normally, a changing to a different brush preset will change all brush "
"settings. Locked presets are a way for you to prevent Krita from changing "
"all settings. So, if you want to have the texture be that same over all "
"brushes, you lock the texture parameter. That way, all brush-preset you "
"select will now share the same texture!"
msgstr ""
"Зазвичай, зміна набору пензлів змінює усі параметри пензля. Заблоковані "
"набори є способом заборонити Krita змінювати усі параметри. Отже, якщо ви "
"хочете, щоб текстура була однаковою для усіх пензлів, слід заблокувати "
"параметр текстури. Після цього усі набори пензлів використовуватимуть "
"однакову текстуру!"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:22
msgid "Locking a brush parameter"
msgstr "Блокування параметра пензля"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:25
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:26
msgid ""
"To lock an option, |mouseright| the little lock icon next to the parameter "
"name, and set it to :guilabel:`Lock`. It will now be highlighted to show "
"it's locked:"
msgstr ""
"Щоб заблокувати параметр, клацніть |mouseright| на малій піктограмі замка "
"поряд із назвою параметра і встановіть для нього значення :guilabel:"
"`Заблокувати`. Пункт параметра буде підсвічено, щоб показати, що його "
"заблоковано:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:29
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:30
msgid "And on the canvas, it will show that the texture-option is locked."
msgstr "І на полотні буде показано, що параметр текстури зафіксовано."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:33
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:35
msgid "Unlocking a brush parameter"
msgstr "Розблокування параметра пензля"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:37
msgid "To *unlock*, |mouseright| the icon again."
msgstr ""
"Щоб «розблокувати» параметр, клацніть |mouseright| на піктограмі ще раз."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:40
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:41
msgid "There will be two options:"
msgstr "Буде запропоновано два варіанти:"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:43
msgid "Unlock (Drop Locked)"
msgstr "Розблокувати (Скинути блокування)"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:44
msgid ""
"This will get rid of the settings of the locked parameter and take that of "
"the active brush preset. So if your brush had no texture on, using this "
"option will revert it to having no texture."
msgstr ""
"Вибір цього варіант призведе до вилучення усіх значень заблокованого "
"параметра і заміни цих значень значеннями активного набору пензлів. Отже, "
"якщо у вашого пензля не було текстури, використання цього варіанта призведе "
"до повернення до варіанта без текстури."

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:46
msgid "This will keep the settings of the parameter even though it's unlocked."
msgstr ""
"Вибір цього варіанта призведе до того, що значення параметра зберігаються, "
"навіть якщо його розблоковано."
