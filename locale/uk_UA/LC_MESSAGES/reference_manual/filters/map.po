# Translation of docs_krita_org_reference_manual___filters___map.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___map\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/filters/map.rst:1
msgid "Overview of the map filters."
msgstr "Огляд фільтрів карти."

#: ../../reference_manual/filters/map.rst:11
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/filters/map.rst:16
msgid "Map"
msgstr "Карта"

#: ../../reference_manual/filters/map.rst:18
msgid "Filters that are signified by them mapping the input image."
msgstr ""
"Фільтри, які характеризуються тим, що створюють своєрідні картки вхідного "
"зображення."

#: ../../reference_manual/filters/map.rst:20
#: ../../reference_manual/filters/map.rst:23
msgid "Small Tiles"
msgstr "Мала плитка"

#: ../../reference_manual/filters/map.rst:20
msgid "Tiles"
msgstr "Плитки"

#: ../../reference_manual/filters/map.rst:25
msgid "Tiles the input image, using its own layer as output."
msgstr ""
"Створює мозаїку з вхідного зображення, використовуючи власний шар для "
"виведення даних."

#: ../../reference_manual/filters/map.rst:27
msgid "Height Map"
msgstr "Карта висот"

#: ../../reference_manual/filters/map.rst:27
msgid "Bumpmap"
msgstr "Рельєф"

#: ../../reference_manual/filters/map.rst:27
msgid "Normal Map"
msgstr "Нормальне картографування"

#: ../../reference_manual/filters/map.rst:30
msgid "Phong Bumpmap"
msgstr "Рельєф за Фонгом"

#: ../../reference_manual/filters/map.rst:33
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/filters/map.rst:34
msgid ""
"Uses the input image as a height-map to output a 3d something, using the "
"phong-lambert shading model. Useful for checking one's height maps during "
"game texturing. Checking the :guilabel:`Normal Map` box will make it use all "
"channels and interpret them as a normal map."
msgstr ""
"Використовує вхідне зображення як карту висот для виведення тривимірного "
"рельєфу, використовуючи модель тіней Фонга-Ламберта. Корисно для перевірки "
"карт висок під час текстурування гри. Позначення пункту :guilabel:`Нормальна "
"карта` призведе до використання усіх каналів кольорів і обробки їх як "
"нормальної карти."

#: ../../reference_manual/filters/map.rst:37
msgid "Round Corners"
msgstr "Заокруглити кути"

#: ../../reference_manual/filters/map.rst:39
msgid "Adds little corners to the input image."
msgstr "Додає малі кутики до вхідного зображення."

#: ../../reference_manual/filters/map.rst:41
#: ../../reference_manual/filters/map.rst:44
msgid "Normalize"
msgstr "Нормалізувати"

#: ../../reference_manual/filters/map.rst:46
msgid ""
"This filter takes the input pixels, puts them into a 3d vector, and then "
"normalizes (makes the vector size exactly 1) the values. This is helpful for "
"normal maps and some minor image-editing functions."
msgstr ""
"Цей фільтр отримує пікселі вхідних даних, розташовує їх у тривимірному "
"векторі, а потім нормалізує значення (робить довжину вектора рівною 1). "
"Корисно для створення нормалізованих карт та деяких незначних функцій із "
"редагування зображень."

#: ../../reference_manual/filters/map.rst:48
#: ../../reference_manual/filters/map.rst:51
msgid "Gradient Map"
msgstr "Карта градієнта"

#: ../../reference_manual/filters/map.rst:48
msgid "Gradient"
msgstr "Градієнт"

#: ../../reference_manual/filters/map.rst:54
msgid ".. image:: images/filters/Krita_filter_gradient_map.png"
msgstr ".. image:: images/filters/Krita_filter_gradient_map.png"

#: ../../reference_manual/filters/map.rst:55
msgid ""
"Maps the lightness of the input to the selected gradient. Useful for fancy "
"artistic effects."
msgstr ""
"Відображає освітленість вхідних даних на вибраних градієнт. Корисно для "
"витончених художніх ефектів."

#: ../../reference_manual/filters/map.rst:57
msgid ""
"In 3.x you could only select predefined gradients. In 4.0, you can select "
"gradients and change them on the fly, as well as use the gradient map filter "
"as a filter layer or filter brush."
msgstr ""
"У версії 3.x ви могли вибирати лише стандартні градієнти. У версії 4.0 ви "
"можете вибрати градієнти і змінити їх на льоту, а також скористатися "
"фільтром відображення градієнтів як шаром фільтрування або пензлем "
"фільтрування."
