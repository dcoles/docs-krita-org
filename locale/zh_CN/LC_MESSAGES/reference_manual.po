msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_reference_manual.pot\n"

#: ../../reference_manual.rst:5
msgid "Reference Manual"
msgstr "参考手册"

#: ../../reference_manual.rst:7
msgid "A quick run-down of all of the tools that are available"
msgstr ""
"参考手册详细介绍了每一个功能的原理和使用方式。由于软件和文档的翻译更新间隔不"
"同，少数选项名称和功能细节可能与最新正式版软件不符。并且由于文档系统的限制，"
"目录和功能均按照各功能的英文名进行排序。如需帮助在 Krita 中显示中文界面，请参"
"考 :ref:`切换应用程序语言 <switch_application_language>` 一节。"

#: ../../reference_manual.rst:9
msgid "Contents:"
msgstr "目录："
