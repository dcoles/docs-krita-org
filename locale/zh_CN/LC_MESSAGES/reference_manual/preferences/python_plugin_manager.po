msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___preferences___python_plugin_manager.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/preferences/python_plugin_manager.rst:None
msgid ""
".. image:: images/preferences/Krita_4_0_preferences_python_plugin_manager.png"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:1
msgid "Python Plugin Manager in Krita."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Python Scripting"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Python"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:10
msgid "Scripts"
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:15
msgid "Python Plugin Manager"
msgstr "Python 插件管理器"

#: ../../reference_manual/preferences/python_plugin_manager.rst:17
msgid "This is part of Krita's python support."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:23
msgid ""
"The python plugin manager can be accessed from :menuselection:`Settings --> "
"Configure Krita --> Python Plugin Manager`. It allows you decide which of "
"the Python Plugins are active."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:25
msgid ""
"It will show you a list of python plugins Krita has found, as well as their "
"description. By default, Python Plugins are disabled, because many python "
"scripts are autostarted, so this ensures only the ones you want to run are "
"being run."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:27
msgid ""
"You can use the checkboxes to toggle them. A restart is required to complete "
"switching off or on the python plugin."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:29
msgid ""
"If you |mouseleft| a plugin, and the plugin has a manual, Krita will display "
"it in the box at the bottom."
msgstr ""

#: ../../reference_manual/preferences/python_plugin_manager.rst:31
msgid ""
"For more information on python, check the :ref:`python scripting category "
"<python_scripting_category>`."
msgstr ""
